package com.ommanisoft.common.config;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.util.MultiValueMap;
import org.springframework.util.MultiValueMapAdapter;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

@Configuration
@Slf4j
public class ExcelConfigRegisterConfig {

  @Bean(name = "ExcelConfigRunner")
  public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
    return args -> {
      try{
        register();
      } catch (Exception e) {
        e.printStackTrace();
      }
    };
  }


  @Async
  public void register() throws IOException {

    log.info("************************** " + "Registering Excel config");
    String host =  Config.getEnvironmentProperty("ommani.static-service");
    if (!StringUtils.hasLength(host)) {
      log.info("************************** " + "Chưa khai báo biến môi trường ommani.static-service");
      return;
    }
    System.out.println(host);
    String path = "/iss_365_static/excel_config/discovery/register_batch";
    String apiUrl = host + path;
    RestTemplate restTemplate = new RestTemplate();

    ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
    Resource[] resources = resolver.getResources("classpath:/excel-config/*");

    if (resources != null && resources.length > 0) {

//    if (folder.isDirectory()) {
      for (Resource resource : resources) {
        try {
          log.info(
              "************************** " +
              "Registering excel config type: " +
                  resource.getFilename());

          MultiValueMap<String, String> headers = new MultiValueMapAdapter<>(new HashMap<>());
          headers.put(
              HttpHeaders.CONTENT_TYPE,
              Collections.singletonList(MediaType.APPLICATION_JSON_VALUE));

          HttpEntity entity =
              new HttpEntity(resource, headers);

          restTemplate.exchange(apiUrl, HttpMethod.POST, entity, String.class);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }
}
