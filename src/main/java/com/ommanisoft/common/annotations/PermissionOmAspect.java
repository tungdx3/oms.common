package com.ommanisoft.common.annotations;

import com.ommanisoft.common.config.Config;
import com.ommanisoft.common.exceptions.ExceptionOm;
import com.ommanisoft.common.utils.RequestUtils;
import com.ommanisoft.common.utils.values.HttpResponse;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Aspect
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PermissionOmAspect {
  @Value("${ommani.sso-service:null}")
  private String ssoApi;

  @Before("@annotation(PermissionOm)")
  public void process(JoinPoint joinPoint) {
    if (ssoApi == null) {
      throw new ExceptionOm(HttpStatus.INTERNAL_SERVER_ERROR, "PermissionOm required ommani.sso-service not null");
    }
    MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    Method method = signature.getMethod();
    PermissionOm om = method.getAnnotation(PermissionOm.class);
    HttpServletRequest request =
      ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    if (!StringUtils.hasLength(request.getHeader(HttpHeaders.AUTHORIZATION))) {
      throw new ExceptionOm(HttpStatus.UNAUTHORIZED, "unauthorized");
    }
    if (om.accepts().length > 0) {
      Map<String, String> requestHeaders = new HashMap<>();
      // check jwt authorization
      requestHeaders.put(HttpHeaders.AUTHORIZATION, request.getHeader(HttpHeaders.AUTHORIZATION));

      HttpResponse res = RequestUtils.sendRequest(HttpMethod.POST,
        ssoApi + "/sso/v1/auth/authorization", om.accepts(), requestHeaders);

      if (res.getStatus() != HttpStatus.OK) {
        throw new ExceptionOm(HttpStatus.INTERNAL_SERVER_ERROR, "Api authorization error");
      }
      long status = Long.parseLong(res.getBody());
      if (status == 401) {
        throw new ExceptionOm(HttpStatus.UNAUTHORIZED, "unauthorized");
      }
      if (status == 403) {
        throw new ExceptionOm(HttpStatus.FORBIDDEN, "forbidden");
      }
    }
  }

  public static boolean hasPermission(String token, List<String> permissions) {
    Map<String, String> requestHeaders = new HashMap<>();
    requestHeaders.put(HttpHeaders.AUTHORIZATION, token);

    System.out.println("===== Config.getEnvironmentProperty: " + Config.getEnvironmentProperty("ommani.sso-service"));
    HttpResponse res = RequestUtils.sendRequest(HttpMethod.POST,
            Config.getEnvironmentProperty("ommani.sso-service") + "/sso/v1/auth/authorization", permissions, requestHeaders);

    if (res.getStatus() != HttpStatus.OK) {
      throw new ExceptionOm(HttpStatus.INTERNAL_SERVER_ERROR, "Api authorization error");
    }

    long status = Long.parseLong(res.getBody());

    return status == 200;
  }
}
