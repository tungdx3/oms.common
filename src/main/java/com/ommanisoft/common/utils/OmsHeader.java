package com.ommanisoft.common.utils;

public class OmsHeader {
  public static final String headerPrefix = "x-om";
  //=======================base============================
  public static final String service = headerPrefix + "-service";
  public static final String headerUserId = headerPrefix + "-user-id";
  public static final String ssoUserId = headerPrefix + "-user-id";
  public static final String name = headerPrefix + "-name";
  public static final String username = headerPrefix + "-username";
  //=======================end base============================

  //=======================fnb============================
  public static final String fnbId = headerPrefix + "-fnb-id";
  public static final String fnbUserId = headerPrefix + "-fnb-user-id";
  public static final String facilityId = headerPrefix + "-facility-id";
  //=======================end fnb============================

  //=======================farm============================
  public static final String farmId = headerPrefix + "-farm-id";
  //=======================end farm============================

  //=======================iss============================
  public static final String enterpriseId = headerPrefix + "-enterprise-id";
  public static final String enterpriseUserId = headerPrefix + "-enterprise-user-id";
  public static final String role = headerPrefix + "-roles";
  public static final String owner = headerPrefix + "-owner";
  public static final String isAdmin = headerPrefix + "-is-admin";
  public static final String organizational = headerPrefix + "-organizational-structure";
  public static final String childMember = headerPrefix + "-child-member";
  public static final String directOrganizational = headerPrefix + "-direct-organizational-structure";
  //=======================end iss============================
  public static final String forwardedFor = "x-forwarded-for";
  public static final String realIp = "x-real-ip";
}
