package com.ommanisoft.common.utils.enums;

import lombok.Getter;

@Getter
public enum OmmaniServiceEnum {

    CHAT_SERVICE("ommani.chat-service", "/iss_365_chat"),
    ACCOUNTANT_SERVICE("ommani.accountant-service", "/iss_365_accountant"),
    CALL_SERVICE("ommani.call-service", "/iss_365_call"),
    DATA_SERVICE("ommani.data-service", "/iss_365_data"),
    LOG_SERVICE("ommani.log-service", "/iss_365_log"),
    MARKETING_SERVICE("ommani.marketing-service", "/iss_365_marketing"),
    PRODUCT_SERVICE("ommani.product-service", "/iss_365_product"),
    PROJECT_SERVICE("ommani.project-service", "/iss_365_project"),
    SALE_SERVICE("ommani.sale-service", "/iss_365_sale"),
    USER_SERVICE("ommani.user-service", "/iss_365_user"),
    NOTIFICATION_SERVICE("ommani.notification-service", "/v1/notification"),
    STATIC_SERVICE("ommani.static-service", "/iss_365_static"),
    SSO_SERVICE("ommani.sso-service", "/sso"),
    ZALO_SERVICE("ommani.zalo-service", "/iss_365_zalo"),
    SETTING_SERVICE("ommani.setting-service", "/iss_365_setting"),
  ;

    private final String environment;
    private final String contextPath;

    OmmaniServiceEnum(String environment, String contextPath) {
      this.environment = environment;
      this.contextPath = contextPath;
    }
}
