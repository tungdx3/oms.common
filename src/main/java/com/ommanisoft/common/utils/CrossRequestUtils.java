package com.ommanisoft.common.utils;

import com.ommanisoft.common.config.Config;
import com.ommanisoft.common.elk.ElkService;
import com.ommanisoft.common.exceptions.ExceptionOm;
import com.ommanisoft.common.utils.enums.OmmaniServiceEnum;
import com.ommanisoft.common.utils.values.HttpResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

import java.util.Map;

@Slf4j
public class CrossRequestUtils {

  public static <T> HttpResponse request(Long userId, OmmaniServiceEnum service, HttpMethod method, String path, T mData, Map<String, String> headerParam) {
    String host =  Config.getEnvironmentProperty(service.getEnvironment());

    if (!StringUtils.hasLength(host)) {
      throw new ExceptionOm(
          HttpStatus.BAD_REQUEST, "Chưa có biến môi trường cho " + service.name());
    }

    String contextPath = service.getContextPath();
    String url = String.format("%s%s%s", host, contextPath, path);

    HttpResponse response = RequestUtils.sendRequest(method, url, mData, headerParam);

    ElkService elkService = new ElkService();

    elkService.whiteLogInternalRequest(
        userId,
        service,
        url,
        method,
        headerParam,
        response.getStatus(),
        mData,
        response
    );

    return response;
  }
}
