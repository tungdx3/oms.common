package com.ommanisoft.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.function.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExportUtils {

  @FunctionalInterface
  public interface Func3<T1,T2,T3,R>{
    R apply(T1 t1,T2 t2,T3 t3);
  }

  /**
   * Hàm này giúp export dữ liệu ra dạng byte
   * @param getDataSupplier Lấy dữ liệu export
   * @param getNotMergeRow Lấy những cột không cần hợp nhất
   * @param createHeaderRowConsumer(headerRow) Tạo ra hàng tiêu đề
   * @param appendDtoToRowConsumer(sheet, item, rowCount) Thêm dữ liệu đến hàng và trả vị trí của hàng cuối cùng
   * @return Byte Trả lại dữ liệu dạng byte
   * @param <T> Loại dữ liệu sử dụng để export
   */
  public static  <T> byte[] export(
      Supplier<List<T>> getDataSupplier,
      Supplier<List<Integer>> getNotMergeRow,
      Consumer<Row> createHeaderRowConsumer,
      Func3<Sheet, T, Integer, Integer> appendDtoToRowConsumer) {
    List<T> items = getDataSupplier.get();

    try (Workbook workbook = new XSSFWorkbook()) {
      Sheet sheet = workbook.createSheet("Dữ liệu");
      Row headerRow = sheet.createRow(0);
      createHeaderRowConsumer.accept(headerRow);

      int firstCell = headerRow.getFirstCellNum();
      int lastCell = headerRow.getLastCellNum();

      int prevRowCount = 1;
      // Vị trí của hàng
      int rowCount = 1;
      List<Integer> notMergeRowData = getNotMergeRow.get();
      for (T item : items) {
        prevRowCount = rowCount;
        rowCount = appendDtoToRowConsumer.apply(sheet, item, rowCount);

        if ((Objects.nonNull(notMergeRowData) && notMergeRowData.isEmpty()) && rowCount > prevRowCount + 1) {
          for (int i = firstCell; i < lastCell; i++) {

            if (!notMergeRowData.contains(i)) {
              CellRangeAddress cellRangeAddress =
                  new CellRangeAddress(prevRowCount, rowCount, i, i);

              sheet.addMergedRegion(cellRangeAddress);

              CellStyle cellStyle = workbook.createCellStyle();
              cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

              Row row = sheet.getRow(prevRowCount);
              Cell cell = row.getCell(i);

              if (Objects.nonNull(cell)) {
                cell.setCellStyle(cellStyle);
              }
            }
          }
        }

        rowCount += 1;
      }

      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      workbook.write(bos);

      return bos.toByteArray();

    } catch (IOException e) {
      throw new RuntimeException("Failed to export data to Excel file: " + e.getMessage());
    }
  }
}
