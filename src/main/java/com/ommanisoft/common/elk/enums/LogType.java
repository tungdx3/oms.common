package com.ommanisoft.common.elk.enums;

public enum LogType {
  OBJECT,
  EXCEPTION,
  REQUEST,
  SQL,
  EVENT,
  API,
  APP
}
