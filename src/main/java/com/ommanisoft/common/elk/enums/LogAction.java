package com.ommanisoft.common.elk.enums;

public enum LogAction {
  CREATE,
  UPDATE,
  DELETE,
  FILTER,
  DETAIL,
  CHANGE_STATE
}
