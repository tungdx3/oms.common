package com.ommanisoft.common.elk.enums;

public enum LogBrowser {
  INTERNET_EXPLORER,
  FIREFOX,
  GOOGLE_CHROME,
  SAFARI,
  OPERA,
  UNKNOWN,
}
