package com.ommanisoft.common.elk.enities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ommanisoft.common.elk.enities.base.LogData;
import com.ommanisoft.common.utils.enums.OmmaniServiceEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
@Data
public class LogRequestData extends LogData {
  private String path;
  private String method;
  private Object params;
  private Object header;
  private Object body;
  private Object response;
  private HttpStatus status;
  private RequestType requestType;
  private OmmaniServiceEnum service;

  @JsonProperty("int_status")
  private Integer intStatus;

  @JsonProperty("time_taken")
  private Long timeTaken;

  @JsonProperty("user_id")
  private Long userId;

  public enum RequestType {
    INTERNAL, EXTERNAL
  }
}
