package com.ommanisoft.common.elk.enities.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;

import com.ommanisoft.common.elk.enums.LogType;
import lombok.Data;

@Data
public class LogData {
  protected LogType type;

  @JsonProperty("created_at")
  protected LocalDateTime createdAt;
}
