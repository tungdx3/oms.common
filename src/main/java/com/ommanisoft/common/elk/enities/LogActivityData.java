package com.ommanisoft.common.elk.enities;


import java.util.Map;

import com.ommanisoft.common.elk.enities.base.LogData;
import com.ommanisoft.common.elk.enums.LogAction;
import lombok.Data;

@Data
public class LogActivityData extends LogData {
  private Long userId;
  private Long mainId;
  private Boolean isSystem = false;
  private LogAction action;
  private Object relatedObject;
  //  private String ipAddress;
  //  private String device;
  //  private String location;
  private Object performedBy;
  private Map<String, Object> extraFields;
}
